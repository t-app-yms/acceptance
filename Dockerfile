FROM cypress/base:10
WORKDIR /app
COPY package*.json ./
RUN yarn install
COPY . .
RUN yarn start