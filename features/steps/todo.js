const {Given, When, Then, setDefaultTimeout} = require("cucumber");
const assert = require('assert').strict;
const openUrl = require("../support/action/openUrl");
const waitForSelector = require("../support/action/waitForSelector")
const clickElement = require("../support/action/clickElement")
const sendKeys = require("../support/action/sendKeys")
const checkElementValue = require("../support/check/checkElementValue")

Given('Empty ToDo list', async function () {
    await openUrl.call(this, "/")
    await waitForSelector.call(this, '.home')
    await checkElementValue.call(this, '#todo-text', false, "") // true === false  - false === true - interesting used :d
});

When(/^I write "([^"]*)" to textBox and click to add button$/, async function (todoDescription) {
    await sendKeys.call(this, "#todo-text", todoDescription)
    await clickElement.call(this, "#add-todo")

});

Then(/^I should see "([^"]*)" item in ToDo list$/, async function (todoDescription) {
    const todoItems = await this.page.$$(".todo");
    for (let todo of todoItems) {
        const todoText = await this.page.evaluate(todo => todo.textContent, todo);
        if(todoText.includes(todoDescription)){
            assert.ok("ok")
            return
        }
    }
    assert.fail()
});