Feature: Todo list
  Scenario: I want to add on my todo list page
  Given Empty ToDo list
  When I write "buy some milk" to textBox and click to add button
  Then I should see "buy some milk" item in ToDo list
