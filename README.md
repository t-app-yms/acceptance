
# ToDo List Application Acceptance Test

## To Do List

- [x] 1- User interface for ONLY adding ToDo’s
- [x] 2- Back-end service to store a persistent state of ToDo list
- [x] 3- Writing deployment files of your front-end and back-end
- [x] 4- Automating build, test and deployment of your application via Gitlab CI/CD pipeline
- [x] 5- Dockerize both your front-end and back-end application to make them ready for deployment.
- [x] 6- Deploy your application to a AWS ECS cloud provider
- [x] 7- Write deployment configurations(i.e. Dockerfile, AWS configure files etc.) for each project.

## Tech Stack For Acceptance

<ol>
<li>Acceptance (E2E Test)
    <ol>
    <li>e2e tests (Cucumber - Puppeteer)</li>
    </ol>
</li>

<li>Gitlab (CI-CD)
    .gitlab-ci.yml 
<ol>
    <li>Test</li>
    </ol>
</li>
</ol>


## Pipeline automation

####Test

![](images/scenario.png)

The script should pass successfully. After configuring the docker so that the test can run, the tests are running.
```
"yarn start"
```


#### Pipeline Layout

![](images/pipeline-acceptance1.png)

![](images/pipeline-acceptance2.png)


Successfully Completed.


## Development enviroment

### Project Setup
To install the packages first

`yarn install`

To run tests

`yarn start`


![](images/tests.png)
